# README #

V souboru `config.yml` je dulezite nakonfigurovat pripojeni k elasticSearch a jelikoz program vyzaduje nainstalovany Chrome, nebo Chromium, je take nutne uvest cestu ke chromedriver. 
Dale je mozne konfigurovat url scrapovane kategorie, nebo zabrazit Chrome s prave scrapovanou strankou `showBrowser: true`.

Vsechny knihovny, ktere python3 vyzaduje jsou uvedeny v souboru `Pipfile`.

## Prikazy
### Spusteni scrapingu
output muze byt ulozen do json souboru nebo elasticSearch indexu:

`python3 alzaScraper.py --scrape --output json`

`python3 alzaScraper.py --scrape --output elastic`

### Vypis dat z elasticSearch
vsechny data:

`python3 alzaScraper.py --showAll`

filtrovani:

 `python3 alzaScraper.py --find "RYZEN 7"`
 
 
