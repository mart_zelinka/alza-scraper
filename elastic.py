import config

from elasticsearch import Elasticsearch, helpers
import json

class ElasticCMD():
    def __init__(self):
        self._es = Elasticsearch(HOST=config.CONFIG["elasticHOST"], PORT=config.CONFIG["elasticPORT"])
        self._es = Elasticsearch()

    def createIndexIfNotExists(self, indexName: str):
        if not self._es.indices.exists(index=indexName):
            print("index created")
            self._es.indices.create(index=indexName)
        else:
            print("index already exists")

    def deleteIndex(self, indexName: str):
        self._es.indices.delete(index=indexName)

    def insertProducts(self, indexName: str, products: list):
        bulk_insert = [
            {
                "_op_type": "index", #inserting data
                "_index": indexName,
                "_source": product,
            } for product in products
        ]

        helpers.bulk(self._es, bulk_insert)

    def readProducts(self, indexName: str):
        result   = helpers.scan(self._es, index=indexName, query={"query":{"match_all":{}}})
       
        for item in result:
            document = json.dumps(item["_source"], indent=4, ensure_ascii=False).encode('UTF-8')
            print("id: {} data: {}".format(item["_id"], document.decode()))

    def searchProducts(self, indexName: str, queryString: str):
        elasticQuery={
            "from": 0,
            "size": 3,
            "query": {
                "multi_match": {
                    "query": queryString,
                    "fields": ["title", "description"]
                    }
                }
            }
        
        result   = self._es.search(index=indexName, body=elasticQuery)
       
        for item in result["hits"]["hits"]:
            document = json.dumps(item["_source"], indent=4, ensure_ascii=False).encode('UTF-8')
            print("id: {} data: {}".format(item["_id"], document.decode()))