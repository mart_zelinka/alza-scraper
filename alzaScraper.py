
from scraper import Scraper
from elastic import ElasticCMD
import config

import time
import json
import argparse
import sys

from pprint import pprint

def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to learn basic argparse')
    parser.add_argument('--scrape',
                        action='store_true',
                        help='mode set as scraping')

    parser.add_argument('--output',
                        help='stroring format of scraping',
                        default='json')

    parser.add_argument('--showAll',
                        action='store_true',
                        help='list all products from elastic')

    parser.add_argument('--find',
                        default=None,
                        help='list all products from elastic')

    results = parser.parse_args(args)
    return (results.scrape, results.output, results.showAll, results.find)


if __name__ == "__main__":

    mode_scrape, arg_output, mode_show_all, mode_find = check_arg(sys.argv[1:])

    if mode_scrape:
        print("Scraping of alza.cz category \"{}\"\n\n".format(config.CONFIG["alzaCategoryName"]))

        scraper = Scraper()
        scraper.start_scrape(firstPage=False)        
        scraper.close()

        if arg_output.lower() == "json":
            print("Storing output of scraping to file: products.json")

            scraper.toJson()
        elif arg_output.lower() == "elastic":
            print("Storing output of scraping to elastic index: {}".format(config.CONFIG["elasticIndex"]))

            el = ElasticCMD()
            el.createIndexIfNotExists(config.CONFIG["elasticIndex"])
            el.insertProducts(config.CONFIG["elasticIndex"], scraper.products)

    elif mode_show_all:
        print("Printing all data from elastic search\n\n")
        
        el = ElasticCMD()
        el.readProducts(config.CONFIG["elasticIndex"])

    elif mode_find:
        print("Searching term {} in elastic\n\n".format(mode_find))

        el = ElasticCMD()
        el.searchProducts(config.CONFIG["elasticIndex"], mode_find)
    
    else:
        print("Invalid arguments")