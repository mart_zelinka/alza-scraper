import yaml
from pprint import pprint

CONFIG = dict()

def loadConfiguration():
    global CONFIG

    with open("config.yml") as yml_file:
        CONFIG = yaml.load(yml_file, Loader=yaml.Loader)

loadConfiguration()

