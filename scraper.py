from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import json
from pprint import pprint

import config

class Scraper():
    def __init__(self):
        options = Options()

        options.add_argument("--disable-blink-features")
        options.add_argument("--disable-blink-features=AutomationControlled")
        
        #no browser window
        if not config.CONFIG["showBrowser"]:
            options.add_argument('--headless')
        
        options.add_argument("start-maximized")

        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        options.add_experimental_option('useAutomationExtension', False)

        self.products = list()
        self.categoryPage = config.CONFIG["alzaCategoryURL"]

        self.__driver = webdriver.Chrome(executable_path=config.CONFIG["chromeDriverPATH"], options=options)

    def start_scrape(self, firstPage=False):
        pageNum = 1
        numOfScrapedProducts = self.scrape_page(self.categoryPage, pageNum)
        if firstPage:
            return

        pageNum += 1
        while numOfScrapedProducts != 0:
            url = "{}-p{}.htm".format(self.categoryPage.rstrip(".htm"), pageNum)
            numOfScrapedProducts = self.scrape_page(url, pageNum)
            pageNum += 1

    def scrape_page(self, page:str, pageNum: int):
        self.__driver.get(page)

        html_elements = self.__driver.find_elements_by_xpath('//div[@class="browsingitemcontainer"]/div[contains(@class, "box") and contains(@class, "browsingitem")]')
        num_of_products  = len(html_elements)
        if num_of_products == 0:
            return num_of_products

        print("Number of products on page num {}. is {}".format(pageNum, num_of_products))

        for element in html_elements:

            product_title_el = element.find_elements_by_xpath('.//div[@class="fb"]/a')[0]

            product_title = product_title_el.text
            product_url  = product_title_el.get_attribute("href")

            product_desc  = element.find_elements_by_xpath('.//div[@class="fb"]/div[@class="Description"]')[0].text

            price_el      = element.find_elements_by_xpath('.//div[@class="bottom"]')[0]
            price_withTax = self.__parse_price(price_el.find_elements_by_xpath('.//div/div/span[@class="c2"]')[0].text)
            price_noTax   = self.__parse_price(price_el.find_elements_by_xpath('.//div/div/span[@class="c1"]')[0].text)

            self.products.append({
                "title": product_title,
                "url":  product_url,
                "description": product_desc,
                "price": price_withTax,
                "price without tax": price_noTax
            })

        return num_of_products

    def toJson(self):
        with open('products.json', 'w') as outfile:
            json.dump(self.products, outfile, indent=4, ensure_ascii=False)

    def close(self):
        self.__driver.quit()

    def __parse_price(self, text: str):
        if text.startswith("bez DPH "):
            text = text[7:]
        
        if text.endswith(",-"):
            text = text[:-2]

        text = text.replace(" ","")
        return int(text)